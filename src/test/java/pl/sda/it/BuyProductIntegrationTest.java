package pl.sda.it;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import pl.sda.model.Product;
import pl.sda.state.InitialState;
import pl.sda.state.State;

import java.math.BigDecimal;

public class BuyProductIntegrationTest {
    @Test
    public void givenThereAreProductsWhenUserWantsToBuyKolaThenProperMessageIsPrinted() {
        //given
        final Product kola = new Product("Kola", BigDecimal.ONE);
        InitialState currentState = new InitialState();
        //when
        final State stateAfterGettingProduct = currentState.pressChoiceOne()
                .putMoney(BigDecimal.ONE)
                .getProduct();
        //then
        assertThat(stateAfterGettingProduct).isInstanceOf(InitialState.class);
    }
}