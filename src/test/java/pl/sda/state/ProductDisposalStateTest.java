package pl.sda.state;

import org.junit.Test;

public class ProductDisposalStateTest {
    @Test(expected = RuntimeException.class)
    public void givenStateIsProductDisposalWhenUserTriesToGetMoneyThenExceptionIsThrown() {
        //given
        final ProductDisposalState stateUnderTest = new ProductDisposalState(null);
        //when
        stateUnderTest.getMoney();
    }

}