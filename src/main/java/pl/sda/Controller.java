package pl.sda;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import pl.sda.state.InitialState;
import pl.sda.state.State;


public class Controller {
    @FXML
    public ImageView vendingMachineImageView;
    @FXML
    private Button button;

    private State currentState;

    public void initialize() {
        currentState = new InitialState();
    }

    public void onImageViewClicked(MouseEvent mouseEvent) {
        System.out.println("Call the police, someone is kicking vending machine!");
    }

    public void onInsertCoinClicked(MouseEvent mouseEvent) {
    }

    public void takeCanClicked(ContextMenuEvent contextMenuEvent) {
    }

    public void takeChangeClicked(MouseEvent mouseEvent) {
    }

    public void onButtonOneClicked(MouseEvent mouseEvent) {
        currentState.pressChoiceOne();
    }
}
