package pl.sda;

import pl.sda.state.InitialState;
import pl.sda.state.State;

import java.math.BigDecimal;
import java.util.Scanner;

public class VendingNonJavaFxMain {
    private static State currentState;
    public static void main(String[] args) {
        //vendingMachineHowTo();
        currentState = new InitialState();
        Scanner scanner = new Scanner(System.in);
        System.out.println("1.Wybierz produkt\n2.Wyciagnij produkt\nCo chcesz zrobic?");
        String userChoice = scanner.nextLine();
        //TODO:
        // 1. walidacja, ze uzytkownik wrzuca 10 zl
        // 2. sensowne obsluzenie wszystkich dosteponych stanow

        while (!"q".equals(userChoice)) {
            if ("1".equals(userChoice)) {
                currentState = currentState.pressChoiceOne();
            } else if ("2".equals(userChoice)) {
                currentState = currentState.getProduct();
            }
            userChoice = scanner.nextLine();
        }
    }

    private static void vendingMachineHowTo() {
        InitialState initialState = new InitialState();

        State selectedProductState = initialState.pressChoiceOne();

        State productDisposalState = selectedProductState.putMoney(BigDecimal.TEN);

        State stateAfterDisposal = productDisposalState.getProduct();

        State state = stateAfterDisposal.pressChoiceOne();
        State state1 = state.putMoney(BigDecimal.TEN);
        State product = state1.getProduct();

        initialState
                .pressChoiceOne()
                .putMoney(BigDecimal.TEN)
                .getProduct();
    }
}
