package pl.sda.state;

import pl.sda.model.Product;

import java.math.BigDecimal;

public class ProductDisposalState implements State {
    private final Product choice;

    public ProductDisposalState(Product choice) {
        this.choice = choice;
    }

    @Override
    public State pressChoiceOne() {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }

    @Override
    public State putMoney(BigDecimal amount) {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }

    @Override
    public State getMoney() {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }

    @Override
    public State getProduct() {
        System.out.println("Here is your product " + choice.getName());
        return new InitialState();
    }
}
