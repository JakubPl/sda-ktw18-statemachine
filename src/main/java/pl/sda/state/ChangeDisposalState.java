package pl.sda.state;

import pl.sda.model.Product;

import java.math.BigDecimal;

public class ChangeDisposalState implements State {
    private final Product choice;
    private final BigDecimal change;

    public ChangeDisposalState(Product choice, BigDecimal change) {
        this.choice = choice;
        this.change = change;
    }

    @Override
    public State pressChoiceOne() {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }

    @Override
    public State putMoney(BigDecimal amount) {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }

    @Override
    public State getMoney() {
        System.out.println("Here is your change: " + change.toString());
        return new ProductDisposalState(choice);
    }

    @Override
    public State getProduct() {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }
}
