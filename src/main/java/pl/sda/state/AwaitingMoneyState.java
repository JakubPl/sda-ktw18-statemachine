package pl.sda.state;

import pl.sda.model.Product;

import java.math.BigDecimal;

public class AwaitingMoneyState implements State {
    private final Product choice;
    private final BigDecimal moneyInside;

    public AwaitingMoneyState(Product choice) {
        this.choice = choice;
        this.moneyInside = BigDecimal.ZERO;
    }

    public AwaitingMoneyState(Product choice, BigDecimal moneyToAdd) {
        this.choice = choice;
        this.moneyInside = moneyToAdd;
    }

    @Override
    public State pressChoiceOne() {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }

    @Override
    public State putMoney(BigDecimal additionalMoney) {
        final BigDecimal newMoneyAmount = this.moneyInside.add(additionalMoney);
        //TODO: check whether user provided enough money and based on that return proper state
        throw new RuntimeException("Not implemented yet!");
    }

    @Override
    public State getMoney() {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }

    @Override
    public State getProduct() {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }
}
