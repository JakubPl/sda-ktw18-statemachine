package pl.sda.state;

import java.math.BigDecimal;

public interface State {
    State pressChoiceOne();
    State putMoney(BigDecimal amount);
    State getMoney();
    State getProduct();
}
