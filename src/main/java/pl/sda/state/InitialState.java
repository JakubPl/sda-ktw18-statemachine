package pl.sda.state;

import pl.sda.model.Product;

import java.math.BigDecimal;

public class InitialState implements State {
    @Override
    public State pressChoiceOne() {
        return new AwaitingMoneyState(new Product("Kola", BigDecimal.TEN));
    }

    @Override
    public State putMoney(BigDecimal amount) {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }

    @Override
    public State getMoney() {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }

    @Override
    public State getProduct() {
        throw new RuntimeException("Handle me somehow more peacefully!");
    }
}
